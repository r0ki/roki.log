# roki.log

ブログ用レポジトリ.

## 構造

* pelican で[構築](https://falgon.github.io/roki.log/posts/2018/%203/22/my_first_post/).
* [pelicanconf.py](./pelicanconf.py) で設定.

## ライセンス

CC-BY-NC-ND

## 規則

* master は[ここ](https://bitbucket.org/r0ki/roki.log), gh-pages は [github](https://github.com/falgon/roki.log). 
* **このリポジトリにプッシュする以外の操作で, `git` コマンドは一切使わない. すべて `Makefile` に設定し, `make` コマンドで行う**.
* **記事を公開する際は, `release` ブランチにプッシュ(基本的に `master` ブランチからのマージ)する**ことで Bitbucket pipeline が実行する. マニュアルで公開する場合は, **必ずサーバーから** `GIT_SSH_COMMAND="ssh -i <keypath>" make github` と実行する.
* テーマは, `./theme`に配下に submodule として追加する.
* 下書きは, `./content/drafts`配下に置く.
* 投稿は, `./content/posts/{year}/{month}/{day}/{slug}/index.md`という形式で置く.
* リソースは, `./content/images/` に置く.
* プラグインは, `./plugins` 配下にサブモジュールとして置く(追加した場合は pelicanconf.py の `PLUGIN_PATH` と `PLUGINS` を適切に設定する必要がある).
* ページは `./content/pages/{page}` に置く.
    * [404 ページは特例として, フォーク済みのテーマに置いてある](https://github.com/falgon/roki.log).
* その他フレームワークなどは, `./frameworks` 配下にサブモジュールとして置き, 随時`make`コマンド一発で適切に更新できるようにする.

## make 設定済みコマンド

| コマンド | 効果 |
| -- | -- |
| `make github` | 自動的に html への出力, ブランチの切断, コミット, プッシュを行う. |
| `make update_submodule` | すべてのサブモジュールを再帰的にアップデートする. |
| `make html` | html へ出力する. |
| `make serve` | localhost:8000 にウェブサーバを立てる. |
| `make devserver` | 開発/監視サーバを localhost:8000 に立てる. `$BASEDIR` 内すべてのファイルが対象. |
| `make stopserver` | 開発/監視サーバを停止する. |
| `make live_preview` | 記事のライブプレビューサーバを localhost:8000 に立てる. `$BASEDIR/content` 内のファイル(markdown)が対象. |
| `make live_preview_open_browser` | `make live_preview` とほぼ変わらないが, サーバ起動時にデフォルトのブラウザを自動で立ち上げる. |
| `make stop_live_preview` | ライブプレビューサーバを停止する. |

## TODO

- [ ] [fatal: the remote end hung up unexpectedly が発生する事案](https://github.com/falgon/nikhil-theme/commit/04eb829be738626dee7289bedee27ea882ab7057)の原因調査
- [ ] Mac で生成されるディレクトリ名が同様の pelicanconf.py を用いているのにも関わらず, Linux (Debian) で実行した場合と異なるディレクトリ名が付与される(月がつく). 恐らく LOCALE の設定か?
