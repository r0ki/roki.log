#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

LOCALE = ('jpn', 'usa', 'ja_JP.UTF-8', 'en_US.UTF-8')
# 'ja_JP.utf8', 'en_US.utf8', 'ja_JP', 'en_US')

DATE_FORMATS = {
        'en': '%a, %d %b %Y',
        'jp': '%Y-%m-%d(%a)',
        #'jp': ('ja_JP.utf8dd', '%Y-%m-%d(%a)'),
        #'en': ('en_US.utf8', '%a, %d %b %Y'),
}

AUTHOR = 'roki'
SITENAME = 'roki.log'
#SITEURL = 'http://localhost:8000'
SITEURL = 'https://falgon.github.io/roki.log'
GITHUB_URL = 'https://github.com/falgon'
TWITTER_USERNAME = '530506'
THEME = './theme/nikhil-theme'
PATH = 'content'
TIMEZONE = 'Japan'
DEFAULT_LANG = 'ja'
TYPOGRIFY = True
DEFAULT_CATEGORY = 'Uncategorised'
USE_FOLDER_AS_CATEGORY = False
DISQUS_SITENAME = 'roki-log'
GOOGLE_ANALYTICS = 'UA-116653080-1'
ARTICLE_EXCLUDES = [ 'drafts' ]
CC_LICENSE = "CC-BY-NC-ND"
GITHUB_USER = "falgon"
SEARCH_URL = SITEURL + '/search.html'

ARTICLE_URL = 'posts/{date:%Y}/{date:%b}/{date:%d}/{slug}/'
ARTICLE_SAVE_AS = 'posts/{date:%Y}/{date:%b}/{date:%d}/{slug}/index.html'
PAGE_URL = 'pages/{slug}/'
PAGE_SAVE_AS = 'pages/{slug}/index.html'
PAGES = [ PAGE_URL ]
DISPLAY_CATEGORIES_ON_MENU = True
SLUG_SUBSTITUTIONS = (('C++', 'cpp'), ('keep dot', 'keep.dot', True))

# Feed generation is usually not desired when developing
FEED_DOMAIN = SITEURL
FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/%s.rss.xml'
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Website', 'https://falgon.github.io/roki/'),
        ('Old blog', 'http://roki.hateblo.jp/'),)

# Social widget
SOCIAL = (('twitter', 'https://twitter.com/530506'),
          ('github', 'https://github.com/falgon'),)

DISPLAY_PAGES_ON_MENU = True
DEFAULT_PAGINATION = 12

MATH_JAX = {
        'color':'black', 
        'responsive':True, 
        'message_style':'None', 
        'tex_extensions': ['color.js', 'enclose.js'], 
        'show_menu': False
}

PLUGIN_PATHS = ['./plugins/pelican-plugins', './plugins']
PLUGINS = ['i18n_subsites', 'render_math', 'tipue_search', 'pelican_dynamic']
JINJA_ENVIRONMENT = { 'extensions': ['jinja2.ext.i18n'], }
DIRECT_TEMPLATES = ['index', 'tags', 'archives', 'search', 'categories', '404']


FAVICON = 'favicon.ico'
FAVICON_TYPE = 'image/vnd.microsoft.icon'
STATIC_PATHS = ['images', 'resources', 'figure']
EXTRA_PATH_METADATA = { 
    'resources/' + FAVICON: {'path': FAVICON}, 
}

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
