Title: About this blog
Date: 2020-03-29 04:50
Category: Uncategorized
Tags: Uncategorized
Slug: about
Author: roki
Summary: About

<style style="display:none">
#rokiframe {
    display: inline-block;
 /*   box-shadow: 0px 0px 20px -5px rgba(0, 0, 0, 0.8);*/
}

.fonticon {
    margin-right: 4px;
}

.abouth2 {
    margin-top: 40px;
}

.titleicon {
    margin-right: 7px;
}

.entry-title {
    text-align: center;
    margin-bottom: 0px;
    font-size: 23.0pt;
}
</style>

<div style="margin-top: -45px; margin-bottom: 44px; text-align: right;">
<span style="position: relative; z-index: 2;">en / <a href="../about_ja">ja</a></span>
</div>

<div class="clearfix">
<div style="">
<img id="rokiframe" class="thumbnail" style="float:left; margin-right:20px;" alt="Profile Image" src="{filename}/images/prof.png" width="200" />
</div>
<i class="fas fa-pencil-ruler fonticon"></i><u>Author</u>: Roki<br>
<i class="fas fa-envelope fonticon"></i><u>Mail</u>: <script type="text/javascript">
<!--
function converter(M){
var str="", str_as="";
for(var i=0;i<M.length;i++){
str_as = M.charCodeAt(i);
str += String.fromCharCode(str_as + 1);
}
return str;
}
function mail_to(k_1,k_2)
{eval(String.fromCharCode(108,111,99,97,116,105,111,110,46,104,114,101,102,32,
61,32,39,109,97,105,108,116,111,58) 
+ escape(k_1) + 
converter(String.fromCharCode(101,96,107,102,110,109,52,50,63,120,96,103,110,110,45,98,110,45,105,111,
62,114,116,97,105,100,98,115,60)) 
+ escape(k_2) + "'");} 
document.write('<a href=JavaScript:mail_to("","")>To inquire<\/a>');
//-->
</script>
<noscript></noscript><br>
<p><i class="fas fa-question-circle fonticon"></i><u>About Content</u>: This is roki blog of learning. I write articles on this blog mainly on technical content. <strong>All opinions expressed by Author on this blog is solely Author's opinions and do not reflect the opinions of the company to which I belong.</strong> For information on various links and accounts that I own, see my website "<a href="https://falgon.github.io/roki/">Roki Profile</a>".
This blog is made up of static site generator <a href="https://github.com/getpelican/pelican">pelican</a>. For the development environment of this blog, see the next article "<a href="https://falgon.github.io/roki.log/posts/2018/%203月/22/my_first_post/">ハローワールド</a>". If you want to write indications or comments please write them in <a href="https://github.com/falgon/roki.log/issues">Github issues</a> or <a href="https://disqus.com/home/forums/roki-log/">DISQUS</a>.</p>

</div>

<script src="https://cdn.rawgit.com/IonicaBizau/github-calendar/gh-pages/dist/github-calendar.min.js"></script>
<link rel="stylesheet" href="https://cdn.rawgit.com/IonicaBizau/github-calendar/gh-pages/dist/github-calendar.css"/>

<h3 class="abouth2"><i class="fas fa-globe titleicon"></i>Browsing environment</h3>
This blog is confirmed the display with the following browser:
<ul style="list-style: none;">
<li><i class="fab fa-chrome fonticon"></i>Google Chrome (latest)</li>
<li><i class="fab fa-firefox fonticon"></i>Firefox (latest stable version, nightly)</li>
<li>Kinza (latest)</li>
</ul>

<h3 class="abouth2"><i class="fas fa-user-secret titleicon"></i>Privacy policy</h3>
This blog collects access logs using Google Analytics and First-Party Cookies for analyzing and improving information. 
Therefore the web browser you are using this blog will automatically send certain information to Google (for example, web address or IP address of the page you visited). 
Also in order to collect that data, Google may set cookies in your browser or read existing cookies. 
The information (statistical information such as the number of accesses, visitor's browser, OS information etc.) obtained by this can be published on this blog and on my social networking account.


<h3 class="abouth2"><i class="fas fa-history titleicon"></i>Change log</h3>
<div class="table-responsive">
<table class="table table-hover" border="0" width="500">
<thead>
<tr>
<th><i class="fas fa-calendar-alt fonticon"></i>YY/MM</th> 
<th><i class="fas fa-book-open fonticon"></i>Content</th>
</tr>
</thead>
<tbody>
<tr>
<td>20/03</td><td><a href="../about_ja">Japanese version</a> added to About page, Disclaimer added</td>
</tr>
<tr>
<td>19/05</td><td>Change the profile image from the <a href="{filename}/images/tux.jpg">Tux</a> image</td>
</tr>
<tr>
<td>18/03</td><td>Moved from Hatena blog to this blog</td>
</tr>
<tr>
<td>16/05</td><td>Start the <a href="https://roki.hateblo.jp/">blog</a> on Hatena blog</td>
</tr>
</tbody>
</table>
</div>

<h3 class="abouth2"><i class="fab fa-twitter titleicon"></i>Tweets</h3>
<a class="twitter-timeline" data-height="400" data-theme="light" href="https://twitter.com/530506?ref_src=twsrc%5Etfw">Tweets by 530506</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/solid.css" integrity="sha384-TbilV5Lbhlwdyc4RuIV/JhD8NR+BfMrvz4BL5QFa2we1hQu6wvREr3v6XSRfCTRp" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/brands.css" integrity="sha384-7xAnn7Zm3QC1jFjVc1A6v/toepoG3JXboQYzbM0jrPzou9OFXm/fY6Z/XiIebl/k" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/fontawesome.css" integrity="sha384-ozJwkrqb90Oa3ZNb+yKFW2lToAWYdTiF1vt8JiH5ptTGHTGcN7qdoR1F95e0kYyG" crossorigin="anonymous">
