Title: このブログについて
Date: 2020-03-29 04:50
Category: Uncategorized
Tags: Uncategorized
Slug: about_ja
Author: roki
Summary: アバウト

<style style="display:none">
#rokiframe {
    display: inline-block;
 /*   box-shadow: 0px 0px 20px -5px rgba(0, 0, 0, 0.8);*/
}

.fonticon {
    margin-right: 4px;
}

.abouth2 {
    margin-top: 40px;
}

.titleicon {
    margin-right: 7px;
}

.entry-title {
    text-align: center;
    margin-bottom: 0px;
    font-size: 23.0pt;
}
</style>

<div style="margin-top: -45px; margin-bottom: 44px; text-align: right;">
<a style="position: relative; z-index: 2;" href="../about">en</a> / ja
</div>
<div class="clearfix">
<div style="">
<img id="rokiframe" class="thumbnail" style="float:left; margin-right:20px;" alt="Profile Image" src="{filename}/images/prof.png" width="200" />
</div>
<i class="fas fa-pencil-ruler fonticon"></i><u>著者</u>: Roki<br>
<i class="fas fa-envelope fonticon"></i><u>メール</u>: <script type="text/javascript">
<!--
function converter(M){
var str="", str_as="";
for(var i=0;i<M.length;i++){
str_as = M.charCodeAt(i);
str += String.fromCharCode(str_as + 1);
}
return str;
}
function mail_to(k_1,k_2)
{eval(String.fromCharCode(108,111,99,97,116,105,111,110,46,104,114,101,102,32,
61,32,39,109,97,105,108,116,111,58) 
+ escape(k_1) + 
converter(String.fromCharCode(101,96,107,102,110,109,52,50,63,120,96,103,110,110,45,98,110,45,105,111,
62,114,116,97,105,100,98,115,60)) 
+ escape(k_2) + "'");} 
document.write('<a href=JavaScript:mail_to("","")>問い合わせる<\/a>');
//-->
</script>
<noscript></noscript><br>
<p><i class="fas fa-question-circle fonticon"></i><u>コンテンツについて</u>: このブログは著者の学習ログを残すブログです. このブログには, 技術的な内容を中心とした記事を執筆します. <strong>このブログで著者が表明したすべての意見は, 著者自身による見解であり, 所属する団体の意見を反映するものではありません. </strong> 私に関連する様々なリンクやアカウントに関する情報は, 私のウェブサイト "<a href="https://falgon.github.io/roki/">Roki Profile</a>" を参照してください.
このブログは静的サイト生成器 <a href="https://github.com/getpelican/pelican">pelican</a> によって作られています. このブログの開発環境に関しては, 次の記事 "<a href="https://falgon.github.io/roki.log/posts/2018/%203月/22/my_first_post/">ハローワールド</a>" をご覧ください. ご指摘やコメントを記入する場合は, <a href="https://github.com/falgon/roki.log/issues">Github issues</a> または各記事の <a href="https://disqus.com/home/forums/roki-log/">DISQUS</a> にお願いします.</p>

</div>

<script src="https://cdn.rawgit.com/IonicaBizau/github-calendar/gh-pages/dist/github-calendar.min.js"></script>
<link rel="stylesheet" href="https://cdn.rawgit.com/IonicaBizau/github-calendar/gh-pages/dist/github-calendar.css"/>

<h3 class="abouth2"><i class="fas fa-globe titleicon"></i>ブラウザ環境</h3>
このブログは, 以下のブラウザで表示の確認をしています:
<ul style="list-style: none;">
<li><i class="fab fa-chrome fonticon"></i>Google Chrome (latest)</li>
<li><i class="fab fa-firefox fonticon"></i>Firefox (latest stable version, nightly)</li>
<li>Kinza (latest)</li>
</ul>

<h3 class="abouth2"><i class="fas fa-user-secret titleicon"></i>プライバシーポリシー</h3>
このブログは, 情報の分析と改善のために, Google Analytics とファーストパーティクッキーを使用して, アクセスログを収集します.
従って, このブログを使用している Web ブラウザは特定の情報
(例えば, アクセスしたページの Web アドレスや IP アドレス) を自動的に Google に送信します.
また, そのデータを収集するために, Google はブラウザにクッキーを設定するか,
既存のクッキーを読み取ることがあります.
これによって取得された情報 (アクセス数, 訪問者のブラウザ, OS 情報などの統計情報) は,
このブログと著者のソーシャルネットワーキングアカウントで公開することがあります.

<h3 class="abouth2"><i class="fas fa-history titleicon"></i>経歴/更新履歴</h3>
<div class="table-responsive">
<table class="table table-hover" border="0" width="500">
<thead>
<tr>
<th><i class="fas fa-calendar-alt fonticon"></i>YY/MM</th> 
<th><i class="fas fa-book-open fonticon"></i>コンテンツ</th>
</tr>
</thead>
<tbody>
<tr>
<td>20/03</td><td>アバウトページに日本語版を追加, 免責事項を新規追加</td>
</tr>
<tr>
<td>19/05</td><td>プロフィール画像を <a href="{filename}/images/tux.jpg">Tux</a> 画像から変更</td>
</tr>
<tr>
<td>18/03</td><td>はてなブログから引っ越し</td>
</tr>
<tr>
<td>16/05</td><td>はてなブログで<a href="https://roki.hateblo.jp/">ブログ</a>を開始</td>
</tr>
</tbody>
</table>
</div>

<h3 class="abouth2"><i class="fab fa-twitter titleicon"></i>ツイート</h3>
<a class="twitter-timeline" data-height="400" data-theme="light" href="https://twitter.com/530506?ref_src=twsrc%5Etfw">Tweets by 530506</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/solid.css" integrity="sha384-TbilV5Lbhlwdyc4RuIV/JhD8NR+BfMrvz4BL5QFa2we1hQu6wvREr3v6XSRfCTRp" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/brands.css" integrity="sha384-7xAnn7Zm3QC1jFjVc1A6v/toepoG3JXboQYzbM0jrPzou9OFXm/fY6Z/XiIebl/k" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/fontawesome.css" integrity="sha384-ozJwkrqb90Oa3ZNb+yKFW2lToAWYdTiF1vt8JiH5ptTGHTGcN7qdoR1F95e0kYyG" crossorigin="anonymous">
